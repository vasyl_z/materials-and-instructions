# Materials and instructions

This repository contains all materials and instructions that will be useful for you during Pivorak Ruby Summer Course 2019

## Table of Contents

### Pre-requirements

Please check the next materials before you'll proceed with your first homework:
- [Pivorak Summer Course Rules](https://docs.google.com/document/d/1uJWZXLPANSxBX2XAQcLRYAYkTz5bpBIXfhQaV3avzyo/edit)
- [Register Gitlab account](https://gitlab.com/users/sign_up)
- [How To Ask Questions The Smart Way](http://www.catb.org/~esr/faqs/smart-questions.html)
- [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/)

### Lectures agenda
| Lecture slides | Homework | Additional materials |
|-----------------|:-------------|:---------------:|
| [Setup ENV](https://docs.google.com/presentation/d/1NgelqJPN9zTP3p5VNu4DKsXFF5WMb2-0rY317BJmA7A/edit?ts=5cf37994#slide=id.p1) | N/A |  pending |
| Ruby Basics | pending | pending |
| Ruby OOP | pending | pending |
| Testing | pending | pending |
| Rails Console | pending | pending |
| Rails HTTP | pending | pending |
| Rails API | pending | pending |
| Design Patterns | pending | pending |

### How to work with homeworks via Gitlab
In order to simplify process of Code Review we'll use gitlab groups.
Each of you received your own unique [gitlab group](https://docs.gitlab.com/ee/user/group/) for RSC 2019 homeworks.

In order to start work on your homework please open specific repository from [homeworks](https://gitlab.com/pivorak-rsc-2019/homeworks) group:
![Open homeworks folder](https://files.g3d.codes/Screen-Shot-2019-06-03-19-33-25-1559579683.png)

When you will open your repository please fork it into your private homework group:
![Click fork button](https://files.g3d.codes/Screen-Shot-2019-06-03-19-38-09-1559579922.png)
![Select your homework gitlab group](https://files.g3d.codes/Screen-Shot-2019-06-03-19-40-40-1559580097.png)

After fork feel free to work on your homework!

Don't implement your solution on master branch in your fork. Checkout to new one - name does not matter.
When your solution is ready create Merge request against master branch in your fork and invite lecturer to review your Merge request

The faster you do this - the more time you will have to improve your work and get higher score